FROM alpine:3

RUN apk add skopeo curl && curl -sSfL https://raw.githubusercontent.com/anchore/grype/main/install.sh | sh -s -- -b /usr/local/bin
# acts as a test
RUN grype version && skopeo -v
